import {
  Controller,
  Post,
  UseInterceptors,
  UploadedFile,
  ParseFilePipeBuilder,
  Get,
} from '@nestjs/common';
import { AmazonS3FileInterceptor } from 'nestjs-multer-extended';
import { AppService } from './app.service';
import { ApiConsumes } from '@nestjs/swagger';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('filename')
  @ApiConsumes(`image/${process.env.FILE_TYPE}`)
  @UseInterceptors(
    AmazonS3FileInterceptor('file', {
      resizeMultiple: [
        { suffix: 'thumb', width: 300, height: 300 },
        { suffix: 'medium', width: 1024, height: 1024 },
        { suffix: 'large', width: 2048, height: 2048 },
      ],
      limits: { fileSize: eval(process.env.MAX_FILE_SIZE) },
      randomFilename: true,
    }),
  )
  uploadFile(
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addFileTypeValidator({
          fileType: process.env.FILE_TYPE,
        })
        .build(),
    )
    file: Express.Multer.File,
  ) {
    return {
      file: file.buffer.toString(),
    };
  }
}
